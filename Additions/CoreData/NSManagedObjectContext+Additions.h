//
//  NSManagedObjectContext(Additions).h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 8.1.14.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSManagedObjectContext (Additions)

- (void) deleteObjects: (id <NSFastEnumeration>) objects;

@end
//
//  UIView+Additions.h
//  utils
//
//  Created by Kostia Dombrovsky on 04.03.11.
//  Copyright 2011 Thomson Reuters. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

- (id) initWithParent:(UIView *)parent;
+ (id) viewWithParent:(UIView *)parent;

// Position of the top-left corner in superview's coordinates
@property CGPoint origin;
@property CGFloat x;
@property CGFloat y;

// Setting size keeps the position (top-left corner) constant
@property CGSize size;
@property CGFloat width;
@property CGFloat height;

@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat bottom;

@property (nonatomic, readonly) UIView* firstResponder;

- (id) superviewOfClass: (Class) aClass;
- (id) subviewOfClass:	  (Class) aClass;
- (id) subviewOfClass:	  (Class) aClass recursive: (BOOL) recursive;
+ (id) subviewOfClass:	  (Class) aClass recursive: (BOOL) recursive inView: (UIView*) aView;
- (NSArray*) subviewsOfClass: (Class) aClass;
- (id) subviewWithTag: (NSInteger) tag;

- (void) printSubviewsRecursive: (BOOL) recursive;
+ (void) printSubviewsInView: (UIView*) aView recursive: (BOOL) recursive;

- (void) insertSubview: (UIView*)view aboveSubviewPassingTest: (BOOL (^)(id obj)) predicate;

- (void) centerInParent;
- (void) centerInParentVertical;
- (void) centerInParentHorizontal;

- (void) centerInParentRounded;

- (void) stretchRight: (CGFloat) rightCoordinate;

- (void) moveAllSubviews: (CGPoint) offset;

- (CGPoint) convertOriginToView: (UIView*) aView;

- (void) translateYByHeight: (BOOL) up;

- (NSComparisonResult) compareByTag: (UIView*) otherView;

@end

//==================================================================================================
/** These category methods execute animation blocks immediately if duration is less or equal to 0 */
@interface UIView (Animations)

+ (void) APAnimateWithDuration: (NSTimeInterval) duration animations: (void (^)(void)) animations;

+ (void) APAnimateWithDuration: (NSTimeInterval) duration animations: (void (^)(void)) animations
                    completion: (void (^)(BOOL finished)) completion;

+ (void) APAnimateWithDuration: (NSTimeInterval) duration options: (UIViewAnimationOptions) options
                  animations: (void (^)(void)) animations;

+ (void) APAnimateWithDuration: (NSTimeInterval) duration delay: (NSTimeInterval) delay
                       options: (UIViewAnimationOptions) options
                    animations: (void (^)(void)) animations
                    completion: (void (^)(BOOL finished)) completion;
@end

//==================================================================================================
@interface UIImageView (MFAdditions)
    + (id) viewWithImageName:  (NSString*) name;
@end

//==================================================================================================
@interface UIAlertView (Extensions)
	+ (id) showAlertWithTitle: (NSString*) aTitle
					  message: (NSString*) aMessage
					 delegate: (id<UIAlertViewDelegate>) aDelegate
			cancelButtonTitle: (NSString*) cancelButtonTitle
			otherButtonTitles :(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

+ (id) showProgressAlertWithTitle: (NSString*) aTitle
						  message: (NSString*) aMessage
						 delegate: (id<UIAlertViewDelegate>) aDelegate
				cancelButtonTitle: (NSString*) cancelButtonTitle
			   otherButtonTitles :(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

@end

//==================================================================================================
@interface UILabel (Extensions)
	@property (nonatomic, readonly) CGFloat textHeight;
@end

//==================================================================================================
@interface UITableView(Additions)
    @property (nonatomic, assign) CGFloat tableHeaderHeight;
    @property (nonatomic, assign) CGFloat tableFooterHeight;

    - (void) insertSectionAtIndex: (NSUInteger) index withRowAnimation: (UITableViewRowAnimation) animation;
    - (void) reloadSectionAtIndex: (NSUInteger) index withRowAnimation: (UITableViewRowAnimation) animation;
    - (void) deleteSectionAtIndex: (NSUInteger) index withRowAnimation: (UITableViewRowAnimation) animation;

    - (id) objectForKeyedSubscript: (id) indexPathOrCell;
@end

//==================================================================================================
@interface UITableViewCell(Extensions)
	+ (id) cellWithStyle: (UITableViewCellStyle) style reuseIdentifier: (NSString*) reuseIdentifier;
    + (id) cellFromNib: (UINib*) nib withReuseIdentifier: (NSString*) reuseIdentifier;
@end

//==================================================================================================
@interface UIBarButtonItem(Extensions)
    + (id) barItemWithSystemItem: (UIBarButtonSystemItem) systemItem target: (id) target action: (SEL) action;
    + (id) itemWithTitle: (NSString*) title style: (UIBarButtonItemStyle) style target: (id) target
                  action: (SEL) action;
    + (id) itemWithCustomView: (UIView*) view;
@end

//==================================================================================================
@interface UISearchBar(Extensions)
    @property (nonatomic, readonly) UIButton* cancelButton;
@end


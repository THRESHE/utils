//
//  NSObject(Additions).m
//  TrueSightApp
//
//  Created by Kostia Dombrovsky on 10.04.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import "NSObject+Additions.h"

@implementation NSObject (Additions)

+ (id) instanceWithXib: (NSString*) xibName
{
	return [self instanceWithXib: xibName owner: nil];
}

+ (id) instanceWithXib: (NSString*) xibName owner: (id) owner
{
	NSArray* elements = [[NSBundle mainBundle] loadNibNamed: xibName owner: owner options: nil];
	for (NSObject* object in elements)
	{
		if ([object isKindOfClass: self.class])
			return object;
	}
	return nil;
}

+ (id) instanceFromXib
{
	return [self instanceWithXib: NSStringFromClass(self.class)];
}

+ (id) instanceFromNib: (UINib*) nib
{
    NSArray* objects = [nib instantiateWithOwner: nil options: nil];
    for (NSObject* object in objects)
        if ([object isKindOfClass: self.class])
            return object;
    return nil;
}

- (void) performBlock: (void (^)(void)) block afterDelay: (NSTimeInterval) delay
{
    int64_t delta = (int64_t) (1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

+ (NSString*) className { return NSStringFromClass(self.class); }
- (NSString*) className { return NSStringFromClass(self.class); }

@end
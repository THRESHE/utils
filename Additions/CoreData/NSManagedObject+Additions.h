//
//  NSManagedObject+Additions.h
//  Perkt_iOS
//
//  Created by Kostia Dombrovsky on 13.02.11.
//  Copyright 2011 DriversEd.com. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Additions)

+ (instancetype) insertedObjectIntoContext: (NSManagedObjectContext*) context;
+ (instancetype) insertedObjectIntoContext: (NSManagedObjectContext*) context
                                withValues: (NSDictionary*) values;
+ (instancetype) temporaryObjectWithEntityFromContext: (NSManagedObjectContext*) context;

@end

//
//  NSIndexPath+Additions.m
//  cinnamon
//
//  Created by Kostia Dombrovsky on 22.04.11.
//  Copyright 2011 Apsmart. All rights reserved.
//

#import "NSIndexPath+Additions.h"

@implementation NSIndexPath(Additions)

- (NSIndexPath*) indexPathForPreviousRow { return [NSIndexPath indexPathForRow: self.row - 1 inSection: self.section]; }
- (NSIndexPath*) indexPathForNextRow     { return [NSIndexPath indexPathForRow: self.row + 1 inSection: self.section]; }

+ (NSArray*) indexPathsStartingWith: (int) start ending: (int) ending section: (int) section
{
    NSMutableArray* array = [NSMutableArray array];
    for (int i = start; i < ending; i++)
    {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow: i inSection: section];
        [array addObject: indexPath];
    }
    return array;
}

@end

//
//  NSManagedObject+Additions.m
//  Perkt_iOS
//
//  Created by Kostia Dombrovsky on 13.02.11.
//  Copyright 2011 DriversEd.com. All rights reserved.
//

#import "NSManagedObject+Additions.h"

@implementation NSManagedObject(Additions)

+ (instancetype) insertedObjectIntoContext: (NSManagedObjectContext*) context
                      withValues: (NSDictionary*) values
{
    NSManagedObject* object = [self.class insertedObjectIntoContext: context];
    object.valuesForKeysWithDictionary = values;
    return object;
}

+ (instancetype) insertedObjectIntoContext: (NSManagedObjectContext*) context
{
    return [NSEntityDescription insertNewObjectForEntityForName: NSStringFromClass([self class])
                                         inManagedObjectContext: context];
}

+ (instancetype) temporaryObjectWithEntityFromContext: (NSManagedObjectContext*) context
{
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName: NSStringFromClass(self.class)
                                                         inManagedObjectContext: context];
    return [[self.class alloc] initWithEntity: entityDescription
               insertIntoManagedObjectContext: nil];
}

@end

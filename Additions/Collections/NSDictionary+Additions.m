//
//  NSDictionary(Additions).m
//  Dependencies
//
//  Created by Kostia Dombrovsky on 10.08.13.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)
@end

@implementation NSMutableDictionary (Additions)

- (BOOL) trySetObject: (id) object forKey: (id <NSCopying>) key
{
    if (object == nil || key == nil)
        return NO;

    [self setObject: object forKey: key];
    return YES;
}

- (void) replaceKey: (id <NSCopying>) oldKey with: (id <NSCopying>) newKey
{
    id value = [self objectForKey: oldKey];
    if (!value)
        return;

    [self setObject: value forKey: newKey];
    [self removeObjectForKey: oldKey];
}

@end
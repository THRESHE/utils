//
//  NSString(Additions).h
//  DriversEd
//
//  Created by Kostia Dombrovsky on 13.03.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString*) trim;
- (BOOL) isNumber;
- (BOOL) isEmail;

@end
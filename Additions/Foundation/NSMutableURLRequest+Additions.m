//
//  NSMutableURLRequest(Additions).m
//  Dependencies
//
//  Created by Kostia Dombrovsky on 10.1.14.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import "NSMutableURLRequest+Additions.h"

@implementation NSMutableURLRequest (Additions)

- (void) setObject: (NSString*) value forKeyedSubscript: (NSString*) headerField
{
    [self setValue: value forHTTPHeaderField: headerField];
}

@end
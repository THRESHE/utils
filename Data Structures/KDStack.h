//
//  KDStack.h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 11.2.14.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KDStack : NSObject
{
    NSMutableArray* _array;
}

- (instancetype) initWithArray: (NSArray*) array;

- (id) pop;
- (id) peek;
- (void) push: (id) obj;

- (NSUInteger) size;

@end
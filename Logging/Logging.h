//
//  Logging.h
//  DriversEd
//
//  Created by Kostia Dombrovsky on 02/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
    #define DLog(fmt, ...) NSLog((fmt), ##__VA_ARGS__);
#else
    #define DLog(...)
#endif

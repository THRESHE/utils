//
//  UIImageView(Additions).h
//  Remoti.co
//
//  Created by Kostia Dombrovsky on 21.01.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Additions)

+ (UIImageView*) withResizableImage: (UIImage*) image
                          capInsets: (UIEdgeInsets) insets
                       resizingMode: (UIImageResizingMode) mode;

- (CGRect) frameForImageAspectFit;
- (CGRect) frameForImageAspectFit: (UIImage*) image;

@end
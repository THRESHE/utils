//
//  UIImage(Additions).m
//  Dependencies
//
//  Created by Kostia Dombrovsky on 3.1.14.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import "UIImage+Additions.h"

@implementation UIImage (Additions)

- (CGFloat) aspectRatio
{
    return self.size.width / self.size.height;
}

+ (UIImage*) imageFromCache: (NSString*) imageName
{
    NSString* path = [@"../Library/Caches/" stringByAppendingString: imageName];
    return [UIImage imageNamed: path];
}

- (BOOL) saveToCache: (NSString*) imageName asRetina: (BOOL) saveAsRetina
{
    NSString* cachesDirectory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];

    if (saveAsRetina)
    {
        NSString* pathExtension = [imageName pathExtension];
        imageName = [NSString stringWithFormat: @"%@@2x.%@", [imageName stringByDeletingPathExtension], pathExtension];
    }

    NSError* error = nil;
    if (![UIImagePNGRepresentation(self) writeToFile: [cachesDirectory stringByAppendingPathComponent: imageName]
                                             options: NSDataWritingAtomic
                                               error: &error])
    {
        NSLog(@"%@", error);
        return NO;
    }

    return YES;
}

@end
//
//  NSOrderedSet(Additions).m
//  Dependencies
//
//  Created by Kostia Dombrovsky on 9/2/13.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import "NSOrderedSet+Additions.h"

@implementation NSOrderedSet (Additions)

- (id) firstObjectPassingTest: (BOOL (^)(id)) predicate
{
    NSUInteger index = [self indexOfObjectPassingTest: ^(id obj, NSUInteger idx, BOOL *stop)
   				                                       {
                                                              *stop = predicate(obj);
   				   	                                       return *stop;
   				                                       }];
   	return index == NSNotFound ? nil : [self objectAtIndex: index];
}

- (id) objectAfter: (id) object
{
    NSUInteger index = [self indexOfObject: object];
    return [self objectAtIndex: index + 1];
}

@end
//
//  NSIndexPath+Additions.h
//  cinnamon
//
//  Created by Kostia Dombrovsky on 22.04.11.
//  Copyright 2011 Apsmart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSIndexPath (Additions)
    - (NSIndexPath*) indexPathForPreviousRow;
    - (NSIndexPath*) indexPathForNextRow;

    + (NSArray*) indexPathsStartingWith: (int) start ending: (int) ending section: (int) section;
@end

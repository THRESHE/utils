//
//  NSDictionary(Additions).h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 10.08.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)
@end

@interface NSMutableDictionary (Additions)

- (BOOL) trySetObject: (id) object forKey: (id <NSCopying>) key;
- (void) replaceKey: (id <NSCopying>) oldKey with: (id <NSCopying>) newKey;

@end
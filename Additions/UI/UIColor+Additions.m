//
//  UIColor(Additions).m
//  DriversEd-iPad
//
//  Created by Kostia Dombrovsky on 09.04.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import "UIColor+Additions.h"

@implementation UIColor (Additions)

+ (UIColor*) colorWithHex: (uint) hex
{
	int red, green, blue, alpha;
	blue  =   hex & 0x000000FF;
	green = ((hex & 0x0000FF00) >> 8);
	red   = ((hex & 0x00FF0000) >> 16);
	alpha = ((hex & 0xFF000000) >> 24);
    return [UIColor colorWithRed: red / 255.0f green: green / 255.0f blue: blue / 255.0f alpha: alpha / 255.0f];
}

+ (UIColor*) colorFromRGBIntegers: (int) red green: (int) green blue: (int) blue alpha: (CGFloat) alpha
{
    CGFloat redF   = red   / 255.0f;
    CGFloat greenF = green / 255.0f;
    CGFloat blueF  = blue  / 255.0f;
    return [UIColor colorWithRed: redF green: greenF blue: blueF alpha: alpha];
}

@end
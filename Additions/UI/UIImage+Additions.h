//
//  UIImage(Additions).h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 3.1.14.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Additions)

@property (nonatomic, assign) CGFloat aspectRatio;

+ (UIImage*) imageFromCache: (NSString*) imageName;

- (BOOL) saveToCache: (NSString*) imageName asRetina: (BOOL) saveAsRetina;

@end
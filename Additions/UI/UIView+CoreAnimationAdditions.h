//
//  UIView(CoreAnimationAdditions).h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 11.2.14.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (CoreAnimationAdditions)

- (void) setAnchorPointMaintainingPosition: (CGPoint) anchorPoint;

@end
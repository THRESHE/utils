//
//  NSMutableURLRequest(Additions).h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 10.1.14.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (Additions)

- (void) setObject: (NSString*) value forKeyedSubscript: (NSString*) headerField;

@end
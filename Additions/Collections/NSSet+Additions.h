//
//  NSSet(Additions).h
//  Onion
//
//  Created by Kostia Dombrovsky on 06.11.12.
//  Copyright (c) 2013 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSSet (Additions)

- (id) anyObjectPassingTest: (BOOL (^)(id)) predicate;
- (BOOL) containsObjectPassingTest: (BOOL (^)(id obj)) predicate;
- (BOOL) containsAllObjectsFrom: (id <NSFastEnumeration>) collection;

@end
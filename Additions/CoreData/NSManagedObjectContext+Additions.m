//
//  NSManagedObjectContext(Additions).m
//  Dependencies
//
//  Created by Kostia Dombrovsky on 8.1.14.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "NSManagedObjectContext+Additions.h"

@implementation NSManagedObjectContext (Additions)

- (void) deleteObjects: (id <NSFastEnumeration>) objects
{
    for (id object in objects)
        [self deleteObject: object];
}

@end
//
//  UILabel(Additions).h
//  Remoti.co
//
//  Created by Kostia Dombrovsky on 22.04.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Additions)

- (UIFont*) adjustedFont;

@end
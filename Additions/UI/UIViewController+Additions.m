//
//  UIViewController(Additions).m
//  DriversEd
//
//  Created by Kostia Dombrovsky on 05.03.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import "UIViewController+Additions.h"

@implementation UIViewController (Additions)

+ (id) controllerFromStoryboard: (UIStoryboard*) storyboard
{
    return [storyboard instantiateViewControllerWithIdentifier: NSStringFromClass(self.class)];
}

@end
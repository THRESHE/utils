//
//  UIViewController(Additions).h
//  DriversEd
//
//  Created by Kostia Dombrovsky on 05.03.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Additions)

+ (id) controllerFromStoryboard: (UIStoryboard*) storyboard;

@end
//
//  UILabel(Additions).m
//  Remoti.co
//
//  Created by Kostia Dombrovsky on 22.04.13.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import "UILabel+Additions.h"

@implementation UILabel (Additions)

- (UIFont*) adjustedFont
{
    CGFloat adjustedFontSize;
    [self.text sizeWithFont: self.font
                minFontSize: self.minimumFontSize
             actualFontSize: &adjustedFontSize
                   forWidth: self.bounds.size.width
              lineBreakMode: self.lineBreakMode];
    return [UIFont fontWithName: self.font.fontName size: adjustedFontSize];
}

@end
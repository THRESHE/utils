//
//  UIColor(Additions).h
//  DriversEd-iPad
//
//  Created by Kostia Dombrovsky on 09.04.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (Additions)

+ (UIColor*) colorWithHex: (uint) hex;
+ (UIColor*) colorFromRGBIntegers: (int) red green: (int) green blue: (int) blue alpha: (CGFloat) alpha;

@end
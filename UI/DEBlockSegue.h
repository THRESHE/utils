//
//  DEBlockSegue.h
//  DriversEd
//
//  Created by Kostia Dombrovsky on 05.03.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DEBlockSegue;

typedef void (^DEBlockSegueBlock)(DEBlockSegue* aSegue);

@interface DEBlockSegue : UIStoryboardSegue
{
}

@property (nonatomic, copy) DEBlockSegueBlock performBlock;

@end
//
//  KDStack.m
//  Dependencies
//
//  Created by Kostia Dombrovsky on 11.2.14.
//  Copyright 2009-2013 Kostia Dombrovsky. All rights reserved.
//

#import "KDStack.h"

@interface KDStack ()
@end

@implementation KDStack

- (id) init
{
    self = [super init];
    if (self)
        _array = [NSMutableArray new];
    return self;
}

- (instancetype) initWithArray: (NSArray*) array
{
    self = [super init];
    if (self)
        _array = array ? [array mutableCopy] : [NSMutableArray new];
    return self;
}

- (id) pop
{
    id result = _array.lastObject;
    if (_array.count)
        [_array removeLastObject];
    return result;
}

- (id) peek { return _array.lastObject; }

- (void) push: (id) obj
{
    [_array addObject: obj];
}

- (NSUInteger) size { return _array.count; }

@end
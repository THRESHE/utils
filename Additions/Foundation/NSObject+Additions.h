//
//  NSObject(Additions).h
//  TrueSightApp
//
//  Created by Kostia Dombrovsky on 10.04.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSObject (Additions)

+ (NSString*) className;
- (NSString*) className;

+ (id) instanceWithXib: (NSString*) xibName;
+ (id) instanceWithXib: (NSString*) xibName owner: (id) owner;
+ (id) instanceFromXib;
+ (id) instanceFromNib: (UINib*) nib;

- (void) performBlock: (void (^)(void)) block afterDelay: (NSTimeInterval) delay;


@end
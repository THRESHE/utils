//
//  NSOrderedSet(Additions).h
//  Dependencies
//
//  Created by Kostia Dombrovsky on 9/2/13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSOrderedSet (Additions)

- (id) firstObjectPassingTest: (BOOL (^)(id)) predicate;

- (id) objectAfter: (id) object;

@end
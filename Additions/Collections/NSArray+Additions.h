//
//  NSArray+Additions.h
//  utils
//
//  Created by Kostia on 02.11.10.
//  Copyright 2010 Thomson Reuters. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(Additions)
	@property (nonatomic, readonly) id firstObject;
    @property (nonatomic, readonly) NSRange range;

- (id)   firstObjectPassingTest: (BOOL (^)(id)) predicate;
- (id)   firstObjectOfClass: (Class) aClass;
- (BOOL) containsObjectPassingTest: (BOOL (^)(id obj)) predicate;
- (NSArray*) objectsPassingTest: (BOOL (^)(id obj, NSUInteger idx, BOOL* stop)) predicate;
- (NSArray*) objectsOfClass: (Class) class;
- (NSUInteger) indexOfObjectBefore: (id) object;
- (id) objectBefore: (id) object;
- (id) objectAfter: (id) object;

- (NSDictionary*) splitIntoSections: (NSString* (^)(id obj)) titlePredicate titles: (NSArray**) titles;

- (NSArray*) arrayByPrependingObject: (id) object;

- (NSArray*) arrayByReplacingObject: (id) object withObject: (id) otherObject;
- (NSArray*) arrayByReplacingObjectPassingTest: (BOOL (^)(id obj)) predicate withObject: (id) object;

- (NSArray*) arrayByRemovingFirstObjectPassingTest:  (BOOL (^)(id obj)) predicate;
- (NSArray*) arrayByRemovingFirstObjectEqualTo: (id) object;

- (NSArray*) subarrayFromIndex: (NSUInteger) index;

- (NSArray*) arrayByTransformingElementsWithBlock: (id (^)(id obj, NSUInteger index)) block;

- (NSArray*) splitIntoSubArraysWithSize: (NSUInteger) size;

- (NSComparisonResult) compareIndexesOfObject: (id) obj andObject: (id) otherObj;

- (NSArray*) shuffledArray;
@end

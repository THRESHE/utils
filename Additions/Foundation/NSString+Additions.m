//
//  NSString(Additions).m
//  DriversEd
//
//  Created by Kostia Dombrovsky on 13.03.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString*) trim
{
    return [self stringByTrimmingCharactersInSet: NSCharacterSet.whitespaceCharacterSet];
}

- (BOOL) isNumber
{
    return [self rangeOfCharacterFromSet: [NSCharacterSet.decimalDigitCharacterSet invertedSet]].location == NSNotFound;
}

- (BOOL) isEmail
{
    NSString* emailRegEx = @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                           @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                           @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                           @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                           @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                           @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                           @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate* regExPredicate = [NSPredicate predicateWithFormat: @"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject: self];
}

@end
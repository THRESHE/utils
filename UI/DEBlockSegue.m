//
//  DEBlockSegue.m
//  DriversEd
//
//  Created by Kostia Dombrovsky on 05.03.13.
//  Copyright 2009-2012 Kostia Dombrovsky. All rights reserved.
//

#import "DEBlockSegue.h"

@interface DEBlockSegue ()
@end

@implementation DEBlockSegue

- (void) perform
{
    if (self.performBlock)
        self.performBlock(self);
    self.performBlock = nil;
}

@end